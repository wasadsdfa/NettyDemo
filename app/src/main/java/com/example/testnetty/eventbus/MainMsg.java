package com.example.testnetty.eventbus;

/**
 * Author:ZJC
 * Date:2020/4/11  14:48
 * Description:MainMsg
 */
public class MainMsg {
    public String msg;

    public MainMsg(String msg) {
        this.msg = msg;
    }
}
